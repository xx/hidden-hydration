# Hidden Hydration

This Minecraft mod simply expands the area where a water block may be in order to hydrate (water) a farmland block.

It adds an additional, 3x1x3 layer _below_ the crop block where water may reside. This makes it possible to create "fields" where water is hidden under the block.

More specifically, it changes the water _scan_ area. Assuming (0, 0, 0) is the position of the farmland block, the scan ranges from:

```
vanilla:  (-4, 0, -4) to (4, 1, 4)
mod:      (-4, 0, -4) to (4, 1, 4)  +  (-2, -1, -2) to (2, -1, 2)
```
