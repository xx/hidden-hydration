package ee.nns.hiddenhydration.mixin;

import net.minecraft.block.FarmlandBlock;
import net.minecraft.registry.tag.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.Iterator;

@Mixin(FarmlandBlock.class)
public class HiddenHydrationMixin {
    @Redirect(
            method = "randomTick(Lnet/minecraft/block/BlockState;Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/util/math/random/Random;)V",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/block/FarmlandBlock;isWaterNearby(Lnet/minecraft/world/WorldView;Lnet/minecraft/util/math/BlockPos;)Z")
    )
    boolean isWaterNearbyInjected(WorldView world, BlockPos pos) {
        Iterator<BlockPos> i = BlockPos.iterate(pos.add(-4, 0, -4), pos.add(4, 1, 4)).iterator();
        Iterator<BlockPos> j = BlockPos.iterate(pos.add(-2, -1, -2), pos.add(2, -1, 2)).iterator();

        BlockPos blockPos;
        do {
            blockPos = i.next();

            if (world.getFluidState(blockPos).isIn(FluidTags.WATER)) {
                return true;
            }
        } while (i.hasNext());

        do {
            blockPos = j.next();

            if (world.getFluidState(blockPos).isIn(FluidTags.WATER)) {
                return true;
            }
        } while (j.hasNext());

        return false;
    }
}
